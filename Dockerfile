FROM ivonatau/tf-keras-nodejs-opencv:latest


COPY ./requirements.txt /app/requirements.txt

WORKDIR /app

RUN pip install -r requirements.txt

COPY . /app

# # Additional settings for Tenserflow Object Detection API
RUN protoc object_detection/protos/*.proto --python_out=.
RUN export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim

# Keras settings
COPY keras.json /root/.keras/keras.json
ENV KERAS_BACKEND=tensorflow
RUN python3 copy_weights.py

EXPOSE 3000


# Install Node.js
RUN cd /nodejs && curl http://nodejs.org/dist/v8.5.0/node-v8.5.0-linux-x64.tar.gz | tar xvzf - -C /nodejs --strip-components=1
ENV PATH $PATH:/nodejs/bin

ENV NPM_CONFIG_LOGLEVEL warn

COPY /front /front

WORKDIR /front

RUN npm install
RUN GENERATE_SOURCEMAP='false' npm run build --prod

RUN cp -r /front/build/index.html /app/app/templates/index.html
RUN cp -r /front/build/static/* /app/app/static/.

WORKDIR /app

CMD python3 run.py

