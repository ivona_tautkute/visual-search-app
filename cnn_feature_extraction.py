"""
Deep visual features extraction using Convolutional Neural Networks.
Resnet50, VGG16 or VGG19 architecture without the top layer, pretrained on Imagenet,
is loaded in Keras and used to extract visual features from images.
"""

import argparse
import numpy as np
import os
import pickle
from PIL import Image

import keras.applications.resnet50
from keras.preprocessing import image
from keras.models import Model, load_model

import index_ranker


cnn_model = keras.applications.resnet50.ResNet50(include_top=False)


class VisualSearchEngine_cnn:
    """
    Class for visual search engine that allows to add new feature vectors and
    look for closest vectors in terms of visual features.
    """
    def __init__(self, image_index):
        self.image_index = image_index

    def add_to_index_cnn(self, image_id, image_path, model_extension):
        """Adds an image's features to engine"""
        print('Adding %s to visual search engine' % image_path)
        features = extract_features_cnn(image_path, model_extension)
        self.image_index[image_id] = features

    def remove_from_index(self, image_id):
        """Removes item with image_id."""
        del self.image_index[image_id]

    def find_similar(self, image_path, model_extension, cnn_features, features_file, n=1):
        """Returns at most n similar images."""
        try:
            query_features = cnn_features[
                os.path.basename(image_path)]
        except KeyError:
            print('Looking for similar images, but first let me extract features - there are no!')
            query_features = save_image_features(image_path, model_extension, features_file)
            cnn_features[os.path.basename(image_path)] = query_features

        return self.image_index.find(query_features, n)



def extract_features_cnn(img_path, model_extension):
    """Returns a normalized features vector for image path and specified model (e.g Resnet)"""
    print('Extracting visual features for', img_path)
    img = image.load_img(img_path, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    if model_extension == "vgg19":
        x = keras.applications.vgg19.preprocess_input(x)
    elif model_extension == "vgg16":
        x = keras.applications.vgg16.preprocess_input(x)
    elif model_extension == "resnet":
        x = keras.applications.resnet50.preprocess_input(x)
    else:
        print('Wrong model name! Using Resnet50 by default.')
        x = keras.applications.resnet50.preprocess_input(x)
    model_features = cnn_model.predict(x, batch_size=1)
    # total_sum = sum(model_features[0]) #Theano
    total_sum = sum(model_features[0][0][0])  # Tensorflow
    features_norm = np.array(
        # [val / total_sum for val in model_features[0]], dtype=np.float32) #Theano
        [val / total_sum for val in model_features[0][0][0]], dtype=np.float32)  # Tensorflow
    if model_extension == "resnet":
        #Reshaping features vectors
        features_norm = features_norm.reshape(2048)
    return features_norm


def save_image_features(img_path, model_extension, features_file):
    """Saves image features vector to pickle file """
    try:
        with open(features_file, 'rb') as handle:
            features_set = pickle.load(handle)
    except FileNotFoundError:
        features_set = {}
    image_name = os.path.basename(img_path)
    if image_name in features_set:
        return features_set[image_name]
    else:
        print(image_name, 'is not in features_set')
        try:
            img = Image.open(img_path)
            query_features = extract_features_cnn(img_path, model_extension)
            features_set[image_name] = query_features
            fileObject = open(features_file, 'wb')
            pickle.dump(features_set, fileObject)
            fileObject.close()
            return query_features
        except OSError:
            print('Cannot open:', img_path)
            return None
        


if __name__ == '__main__':
    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'directory',
        help="Directory of images to be processed for CNN feature extraction")
    parser.add_argument(
        'model_extension',
        help="Architecture for feature extraction: vgg16, vgg19 or resnet")
    parser.add_argument(
        'cnn_layer',
        help="Convolutional neural network layer used to extract features, e.g. fc2 (see: Keras documentation)")
    args = parser.parse_args()

    #Initiate required model
    if args.model_extension == "vgg19":
        base_model = keras.applications.vgg19.VGG19(weights='imagenet')
        cnn_model = Model(input=base_model.input, output=base_model.get_layer(
            args.cnn_layer).output)
    elif args.model_extension == "vgg16":
        base_model = keras.applications.vgg16.VGG16(weights='imagenet')
        cnn_model = Model(input=base_model.input, output=base_model.get_layer(
            args.cnn_layer).output)
    elif args.model_extension == "resnet":
        cnn_model = keras.applications.resnet50.ResNet50(include_top=False)
    else:
        print('Not a CNN feature extraction model')

    #Extract visual features for all images in the directory and save them to .p file
    for filename in os.listdir(args.directory):
        save_image_features(os.path.join(args.directory, filename), args.model_extension,
                            'pickles/cnn_features_' + args.model_extension + '.pickle')
