"""Functions to build image retrieval engine """

import csv
import logging.config
import operator
import os
import pickle
import time
from PIL import Image
import yaml

from cnn_feature_extraction import VisualSearchEngine_cnn
from index_ranker import SimpleRanker, Intersection, InvertedIndex


def timeit(func):
    """Measure function running time"""
    def timed(*args, **kw):
        ts = time.time()
        result = func(*args, **kw)
        te = time.time()
        print('%s was completed in %2.2f sec' %
                     (func.__name__, te - ts))
        return result
    return timed


def read_image(filename, directory):
    """Reads image from the given directory"""
    if filename is not None:
        ndir = os.path.join(directory, filename)
        image = cv2.imread(ndir, cv2.IMREAD_GRAYSCALE)
        if image is not None:
            return image
        else:
            logging.debug('No such image %s in given directory' % (filename))


def load(filename):
    """Loads data from file using pickle."""
    with open(filename, 'rb') as file:
        print('Opening:', filename)
        return pickle.load(file)

@timeit
def cnn_descriptor(
        directory,
        features_clusters,
        model_extension):
    ranker = SimpleRanker(hist_comparator=Intersection())
    index = InvertedIndex(ranker=ranker,
                              recognized_visual_words=features_clusters)
    vse_engine = VisualSearchEngine_cnn(index)
    for filename in os.listdir(directory):
        ndir = os.path.join(directory, filename)
        try:
            image = Image.open(ndir)
            vse_engine.add_to_index_cnn(filename, ndir, model_extension)
        except OSError:
            print('Not an image', ndir)
            continue
    return vse_engine

@timeit
def initiate_engine(results_dir, feature_model):
    if feature_model == "bovw":
        print('BOVW model not supported in web app!')
    else:
        if feature_model == "resnet":
            features_dim = 2048
        else:
            features_dim = 4096
        vse_engine = cnn_descriptor(
            results_dir,
            features_dim,
            feature_model)
        file_name = os.path.join('pickles/', os.path.basename(results_dir) +'_' + feature_model + '.pickle')
        fileObject = open(file_name,'wb') 
        pickle.dump(vse_engine,fileObject)
        fileObject.close()
    return vse_engine


@timeit
def return_similar(filename, results_dir, engine, model_extension, cnn_features, features_file, nb_matches):
    """Main function for returning similar images"""
    test_image = Image.open(filename)
    result = engine.find_similar(filename, model_extension, cnn_features, features_file, nb_matches)  
    best_result = {}
    for entry in result:
        best_result[entry[0]] = 0
    return best_result
    # except OSError:
    #     print('No such file!', filename)
    #     return ('not_found.jpg', 0)



