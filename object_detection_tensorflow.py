"""
Object detection using Tensorflow Object Detection API 
https://github.com/tensorflow/models/tree/master/object_detection
Loads a trained Tensorflow model and detects bounding boxes for
given object categories on query image.
"""
import numpy as np
import os
import pickle
import scipy.misc
import tensorflow as tf
import time

from PIL import Image

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util


def timeit(func):
    """Measure function running time"""
    def timed(*args, **kw):
        ts = time.time()
        result = func(*args, **kw)
        te = time.time()
        print('%s was completed in %2.2f sec' %
                     (func.__name__, te - ts))
        return result
    return timed

@timeit
def load_image_into_numpy_array(input_image):
    """Takes image in PIL format and returns a numpy array"""
    image = input_image.convert('RGB')
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 3)).astype(np.uint8)


@timeit
def rescale_bounding_boxes(filename, xmin, xmax, ymin, ymax):
    """Rescales normalized bounding box coordinates to pixelwise values"""
    image = Image.open(filename)
    im_width, im_height = image.size
    (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                  ymin * im_height, ymax * im_height)
    return (left, right, top, bottom)


@timeit
def load_object_detection_model(path_to_pb_model):
    """Loads a frozen Tensorflow model. Returns detection graph"""
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(path_to_pb_model, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
    return detection_graph

@timeit
def load_labels_map(path_to_labels_map,
                    nb_of_classes):
    """Load labels map and returns a category index"""
    label_map = label_map_util.load_labelmap(path_to_labels_map)
    categories = label_map_util.convert_label_map_to_categories(label_map,
                                                                max_num_classes=nb_of_classes,
                                                                use_display_name=True)
    category_index = label_map_util.create_category_index(categories)
    return category_index

@timeit
def run_object_detection(filename,
                         detection_graph,
                         category_index,
                         output_folder,
                         min_score_thresh,
                         nb_of_classes=3,
                         line_thickness = 10,
                         bounding_boxes_file='pickles/bounding_boxes.pickle'):
    """Takes filename as an input,
    runs Tensorflow Object Detection and
    returns bounding boxes as well as saves predictions image"""
    print('Running object detection for:', filename)
    class_name = ""
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            image = Image.open(filename)
            image_np = load_image_into_numpy_array(image)
            image_np_expanded = np.expand_dims(image_np, axis=0)
            image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
            boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
            scores = detection_graph.get_tensor_by_name('detection_scores:0')
            classes = detection_graph.get_tensor_by_name('detection_classes:0')
            num_detections = detection_graph.get_tensor_by_name(
                'num_detections:0')
            (boxes, scores, classes, num_detections) = sess.run(
                [boxes, scores, classes, num_detections],
                feed_dict={image_tensor: image_np_expanded})
            bounding_boxes = []
            for i in range(np.squeeze(boxes).shape[0]):
                if np.squeeze(scores) is None or np.squeeze(scores)[i] > min_score_thresh:
                    box = tuple(np.squeeze(boxes)[i].tolist())
                    if np.squeeze(classes).astype(np.int32)[i] in category_index.keys():
                        class_name = category_index[np.squeeze(classes)[i]]['name']
                        prob = np.squeeze(scores)[i]
                        print('class name:', class_name)
                        print('score',prob)
                    ymin, xmin, ymax, xmax = box
                    (left, right, top, bottom) = rescale_bounding_boxes(
                        filename, xmin, xmax, ymin, ymax)
                    bounding_boxes.append([class_name, left, right, top, bottom, prob])
            image = Image.open(filename)
            image_np = load_image_into_numpy_array(image)
            vis_util.visualize_boxes_and_labels_on_image_array(
                image_np,
                np.squeeze(boxes),
                np.squeeze(classes).astype(np.int32),
                np.squeeze(scores),
                category_index,
                use_normalized_coordinates=True,
                min_score_thresh=min_score_thresh,
                line_thickness=line_thickness)
            scipy.misc.imsave(os.path.join(
                output_folder, 'predictions_' + os.path.basename(filename)), image_np)

            #save the detections
            try:
                with open(bounding_boxes_file, 'rb') as handle:
                    detected_bounding_boxes = pickle.load(handle)
            except FileNotFoundError:
                detected_bounding_boxes = {}

            detected_bounding_boxes[os.path.basename(filename)] = bounding_boxes
            fileObject = open(bounding_boxes_file, 'wb')
            pickle.dump(detected_bounding_boxes, fileObject)
            fileObject.close()

            return bounding_boxes

@timeit
def get_bounding_boxes(filename,
                       category_index,
                       detection_graph,
                       output_folder,
                       min_score_thresh,
                       bounding_boxes_file='pickles/bounding_boxes.pickle'):
    """
    Takes image path, category index from load_labels_map() and detection graph
    from load_object_detection_model()
    returns coordinates for bounding boxes that meet the min detection threshold,
    saves detected bounding boxes to pickle file and saves predictions image do folder.
    """
    try:
        with open(bounding_boxes_file, 'rb') as handle:
            detected_bounding_boxes = pickle.load(handle)
    except FileNotFoundError:
        detected_bounding_boxes = {}
    if os.path.basename(filename) in detected_bounding_boxes:
        return detected_bounding_boxes[os.path.basename(filename)]
    else:
        bounding_boxes = run_object_detection(filename,
                                              detection_graph,
                                              category_index,
                                              output_folder,
                                              min_score_thresh)
        detected_bounding_boxes[os.path.basename(filename)] = bounding_boxes
        fileObject = open(bounding_boxes_file, 'wb')
        pickle.dump(detected_bounding_boxes, fileObject)
        fileObject.close()
        return bounding_boxes

@timeit
def get_most_probable_category(filename,
                               category_index,
                               detection_graph,
                               output_folder,
                               min_score_thresh,
                               allowed_categories=['bed', 'chair', 'clock', 'couch', 'dining table']):
  """
  Takes path to image, object detection model, output folder where to save the detection images,
  minimal threshold for detection.
  Returns object class with the highest score for predictions on given image. Saves cropped image
  to output folder.
  """
  bb = get_bounding_boxes(filename,
                          category_index,
                          detection_graph,
                          output_folder,
                          min_score_thresh)
  max_score = 0
  max_class = ""
  max_box = []
  for box in bb:
    if box[5] > max_score and box[0] in allowed_categories:
      max_score = box[5]
      max_class = box[0]
      max_box = box
  bounding_box_image_path = save_cropped_bounding_box(filename,
                            max_box,
                            output_folder)
  return max_class, bounding_box_image_path

@timeit
def save_predictions(filename,
                     output_folder,
                     category_index,
                     detection_graph,
                     min_score_thresh,
                     bounding_boxes_file='pickles/bounding_boxes.pickle',
                     use_normalized_coordinates=True,
                     line_thickness=10):
    """Runs Tensorflow Object Detection and
    creates a predictions .JPG file in destination folder"""
    bounding_boxes = get_bounding_boxes(filename,
                                        category_index,
                                        detection_graph,
                                        output_folder,
                                        min_score_thresh,
                                        bounding_boxes_file)
    print ('got bounding boxes:', bounding_boxes)
    for box in bounding_boxes:
        save_cropped_bounding_box(filename,
                                  box,
                                  output_folder,
                                  use_normalized_coordinates=True)


@timeit
def save_cropped_bounding_box(filename,
                              bounding_box,
                              out_folder,
                              use_normalized_coordinates=True):
    """Takes path to image, bounding box and saves a cropped image in output folder."""
    object_class, xmin, xmax, ymin, ymax, score = bounding_box
    image = Image.open(filename)
    cropped_image = image.crop((xmin, ymin, xmax, ymax))
    cropped_image_path = os.path.join(
                out_folder,
                object_class + '_' + str(int(xmin)) + '_' + str(int(xmax)) + '_' + str(int(ymin)) + '_' + str(int(ymax)) + '_' + os.path.basename(filename))
    if os.path.isfile(cropped_image_path):
        print(os.path.basename(cropped_image_path), 'already exists')
    else:
        cropped_image.save(cropped_image_path)
    return cropped_image_path


if __name__ == '__main__':
    category_index = load_labels_map('object_detection/data/mscoco_label_map.pbtxt',
                                     nb_of_classes=90)
    detection_graph = load_object_detection_model(
        'object_detection/ssd_inception_coco.pb')

    