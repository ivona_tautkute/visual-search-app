"""
Functions for comparing histograms and returning the most similar.
Based on OpenCV.
"""

import abc
from abc import ABCMeta, abstractmethod
import cv2
import heapq
import operator


class HistComparator(metaclass=abc.ABCMeta):
    reversed = False

    @abstractmethod
    def compare(self, h1, h2):
        """"Compares histograms. Returns comparison metric"""
        pass

class Intersection(HistComparator):
    reversed = True


    def compare(self, h1, h2):
        return cv2.compareHist(h1, h2, cv2.HISTCMP_INTERSECT)


class Ranker(metaclass=abc.ABCMeta):
    def __init__(self, hist_comparator):
        self.hist_comparator = hist_comparator

    @abstractmethod
    def rank(self, query_hist, items, n, freq_vector):
        """Ranks index items by similarity to query_hist. Returns list of tuples: (image_id, diff_ratio)."""
        pass

    def _rank_best_results(self, items, n, diff_ratio_function):
        results = [(image_id, diff_ratio_function(hist)) for image_id, hist in items]
        return self._n_best_results(results, n)

    def _n_best_results(self, results, n):
        if self.hist_comparator.reversed:
            function = heapq.nlargest
        else:
            function = heapq.nsmallest
        return function(n, results, key=operator.itemgetter(1))


class SimpleRanker(Ranker):
    def __init__(self, hist_comparator):
        Ranker.__init__(self, hist_comparator)

    def rank(self, query_hist, items, n, freq_vector=None):

        def diff_ratio_function(hist):
            return self.hist_comparator.compare(hist, query_hist)

        return self._rank_best_results(items, n, diff_ratio_function)


class Index:
    def __init__(self, ranker):
        self.ranker = ranker
        self.vw_freq = []

    @abc.abstractmethod
    def find(self, query_hist, n):
        pass

    def __setitem__(self, image_id, hist):
        self._add(image_id, hist)
        self._update_freq_after_addition(hist)

    @abc.abstractmethod
    def _add(self, image_id, hist):
        pass

    def _update_freq_after_addition(self, hist):
        if not self.vw_freq:
            self.vw_freq = hist.copy()
        index_size = len(self)
        self.vw_freq = [(n_freq * (index_size - 1) + n) / index_size for n, n_freq in zip(hist, self.vw_freq)]

    def __delitem__(self, image_id):
        hist = self[image_id]
        self._remove(image_id)
        self._update_freq_after_deletion(hist)

    @abc.abstractmethod
    def _remove(self, image_id):
        pass

    def _update_freq_after_deletion(self, hist):
        index_size = len(self)
        if index_size == 0:
            self.vw_freq = []
        self.vw_freq = [(n_freq * (index_size + 1) - n) / index_size for n, n_freq in zip(hist, self.vw_freq)]

    @abc.abstractmethod
    def __getitem__(self, image_id):
        pass

    @abc.abstractmethod
    def __len__(self):
        pass

class InvertedIndex(Index):
    def __init__(self, ranker, recognized_visual_words, cutoff=2.0):
        Index.__init__(self, ranker)
        self.index = [{} for i in range(recognized_visual_words)]
        self.cutoff = cutoff / recognized_visual_words

    def find(self, query_hist, n):
        return self.ranker.rank(query_hist, self._items(query_hist), n, self.vw_freq)

    def _items(self, query_hist):
        results = {}
        for visual_word_freq, subindex in zip(query_hist, self.index):
            if visual_word_freq > self.cutoff:
                results.update(subindex)
        return results.items()

    def _add(self, image_id, hist):
        for visual_word_freq, subindex in zip(hist, self.index):
            if visual_word_freq > self.cutoff:
                if image_id in subindex:
                    raise DuplicatedImageError(image_id)
                subindex[image_id] = hist

    def _remove(self, image_id):
        found = False
        for subindex in self.index:
            if image_id in subindex:
                found = True
                del subindex[image_id]
        if not found:
            raise NoImageError(image_id)

    def __getitem__(self, image_id):
        for subindex in self.index:
            if image_id in subindex:
                return subindex[image_id]
        raise KeyError(image_id)

    def __len__(self):
        return len(set(image_id for subindex in self.index for image_id in subindex))