// requirements
const autoprefixer = require('gulp-autoprefixer'),
      concat = require('gulp-concat'),
      del = require('del'),
      gulp = require('gulp'),
      minify = require('gulp-minify-css'),
      plumber = require('gulp-plumber'),
      rename = require('gulp-rename'),
      uglify = require('gulp-uglify'),
      sass = require('gulp-sass');


// Delete
gulp.task('del', () => {
  return del(['./app/static/style/main.min.css']);
});

// Sass
gulp.task('sass', () => {
  return gulp.src('./app/static/style/scss/main.scss')
  .pipe(plumber())
  .pipe(sass())
  .pipe(autoprefixer('last 2 versions'))
  .pipe(minify({keepBreaks: true}))
  .pipe(rename({
    suffix: '.min'
  }))
  .pipe(gulp.dest('./app/static/style'));
});

// Scripts-UI
gulp.task('scripts', () => {
  gulp.src('./app/static/js/all/*.js')
  .pipe(concat('index.min.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./app/static/js'));
});

gulp.task('default', ['del','sass','scripts']);

gulp.task('watch', () => {
  gulp.watch('./app/static/style/scss/*.scss', ['sass']);
  gulp.watch('./app/static/js/all/*.js', ['scripts']);
});
