# Visual Search for Interior Design - demo app

This repo contains code for style search app on interior design items.

# Dependencies

All dependencies are managed in Dockerfile. Installed Python libraries are contained in file `requirements.txt`.

* Python 3
* Tensorflow Object Detection API (https://github.com/tensorflow/models/blob/master/object_detection/README.md)
* Open CV (http://opencv.org)
* Keras (https://keras.io)
* Node.js


# Instructions


* To start the app in Docker container, build and run the container.
	`docker build -t container-name .`
	`docker run -p 3000:3000 -it container-name`

* To start the app in Docker need to run this by docker-compose.
	`docker-compose up`

* To install `npm` dependencies and generate minificated files run:
	`npm install`
	`npm install --global gulp-cli`
	`gulp`

* To run the app locally, install all the dependencies and start with command:
	`python3 run.py`


App is set up to run on host 3000.

# Object detection

SSD MobileNet architecture (https://github.com/tensorflow/models/blob/master/object_detection/g3doc/detection_model_zoo.md)

# Visual features extraction

Visual features are extracted in Keras/Tensorflow. Pretrained model is used with Resnet 50 architecture (without the top layer) that was trained on Imagenet. Feature extraction functions are contained in `cnn_feature_extraction.py` file.

# Web interface

Prototype demo app was built in Python Flask. Web pages templates are available in `app/templates`. Interface is managed via routes in `app/web_interface.py` file.
