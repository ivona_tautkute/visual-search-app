import axios from 'axios';

const API_URL = process.env.API_URL;

export const fetchGallery = () => ({
  type: 'FETCH_GALLERY',
  payload: axios.get(`${API_URL}/api/v1/scenes`)
});

export const fetchInspirations = (filename) => ({
  type: 'FETCH_INSPIRATIONS',
  payload: axios.get(`${API_URL}/api/v1/scenes/${filename}`)
});

export const setCategory = (category) => ({
  type: 'SET_CATEGORY_FILTER',
  payload: category
});

export const showCategoryFilter = () => ({
  type: 'SHOW_CATEGORY_FILTER',
});

export const hideCategoryFilter = () => ({
  type: 'HIDE_CATEGORY_FILTER',
});

export const selectInspirationItem = (item) => ({
  type: 'SELECT_INSPIRATION_ITEM',
  payload: item
});


export const fetchSimilarObjects = (filename) => ({
  type: 'FETCH_SIMILAR_OBJECTS',
  payload: axios.get(`${API_URL}/api/v1/similar/${filename}`)
});

export const fetchComplementaryObjects = (filename) => ({
  type: 'FETCH_COMPLEMENTARY_OBJECTS',
  payload: axios.get(`${API_URL}/api/v1/complementary/${filename}`)
});

export const showUploader = () => ({
  type: 'SHOW_UPLOADER'
});

export const hideUploader = () => ({
  type: 'HIDE_UPLOADER'
});

export const uploadFile = (formData) => ({
  type: 'UPLOAD_FILE',
  payload: axios.post(`${API_URL}/api/v1/scenes`, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
});

export const searchRefinedObjects = (filename, searchText) => ({
  type: 'SEARCH_REFINED_OBJECTS',
  payload: axios.get(`${API_URL}/api/v1/refined/${filename}/${searchText}`)
})
