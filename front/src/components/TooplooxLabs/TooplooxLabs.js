import React, { Component } from 'react';
import { connect } from 'react-redux';
import './TooplooxLabs.scss';
import logo from '../../assets/tplx-lab.svg';

const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch =>{
  return {}
}

class TooplooxLabs extends Component {
  render() {
    return (
      <div className="TooplooxLabs">
        <div className="logo">
          <a href="/">
            <img src={logo} alt="Logo" />
          </a>
        </div>
        <div className="contact-btn">
          <a href="http://tooploox.com#contact">LET'S TALK</a>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TooplooxLabs);
