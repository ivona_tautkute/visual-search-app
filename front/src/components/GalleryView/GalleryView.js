import React, { Component } from 'react';
import { connect } from 'react-redux';
import './GalleryView.scss';
import Gallery from '../Gallery/Gallery';
import CategoryFilter from '../CategoryFilter/CategoryFilter';

const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch =>{
  return {}
}

class GalleryView extends Component {

  render() {
    return (
      <div className="GalleryView">
        <div className="gallery-header">
          <div className="header-text">
            <h3>Inspirations gallery</h3>
            We will detect furniture items and look for similar ones in our database.
          </div>
          <div className="header-actions">
            <CategoryFilter />
          </div>
        </div>
        <Gallery />
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(GalleryView);
