import React, { Component } from 'react';
import './InspirationView.scss';
import store from '../../store';
import { fetchInspirations } from '../../actions/inspirationActions';
import Gallery from '../Gallery/Gallery';
import Inspiration from '../Inspiration/Inspiration';
import SearchReady from '../SearchReady/SearchReady';
import SearchResults from '../SearchResults/SearchResults';


class InspirationView extends Component {

  constructor(props) {
    super(props);
    this.state = {
      foundItems: [],
      framedImage: null,
      queryImage: null
      }

    this.unsubscribe = store.subscribe(() => {
      const state = store.getState();
        if (state.inspirationsReducer.inspiration.data) {
          this.setState({
            foundItems: state.inspirationsReducer.inspiration.data.objects,
            framedImage: state.inspirationsReducer.inspiration.data.framedImage,
            queryImage: state.inspirationsReducer.inspiration.data.queryImage,
            fetching: state.inspirationsReducer.inspiration.fetching,
            selectedItem: state.inspirationsReducer.inspiration.item,
          })
        } else {
          this.setState({
            fetching: state.inspirationsReducer.inspiration.fetching,
            selectedItem: state.inspirationsReducer.inspiration.item,
          })
        }
    })
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentWillReceiveProps(next) {
    if(this.props.match.params.image) {
      store.dispatch(fetchInspirations(next.match.params.image));
    }
  }

  componentDidMount() {
    if(this.props.match.params.image) {
      store.dispatch(fetchInspirations(this.props.match.params.image));
    }
  }

  render() {
    return (
      <div className="InspirationView" data-sticky-container>
        <div className="inspirations-container">
          <div className="inspirations-section">
            <h4>We have found {this.state.foundItems.length} item(s) in the picture</h4>
            <Inspiration image={this.props.match.params.image} />
          </div>
          <div className="gallery-section">
            <h3>More {this.state.categoryFilter}</h3>
            <Gallery />
          </div>
        </div>

        <div className="items-container" data-margin-top="100">
        {!this.state.selectedItem &&
          <SearchReady />
        }
        {this.state.selectedItem &&
          <SearchResults />
        }
        </div>
      </div>
    );
  }
}

export default InspirationView;
