import React from 'react';
import ReactDOM from 'react-dom';
import Inspiration from './Inspiration';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Inspiration />, div);
});
