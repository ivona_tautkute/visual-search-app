import React, { Component } from 'react';
import './Uploader.scss';
import store from '../../store';
import { fetchGallery } from '../../actions/inspirationActions';
import Tile from '../Tile/Tile';
import chairImg from '../../assets/chair.svg';
import { connect } from 'react-redux';
import { hideUploader, uploadFile } from '../../actions/inspirationActions';
import history from '../../history';

const mapStateToProps = state => {
  return { visible: state.inspirationsReducer.uploader.visible };
}

const mapDispatchToProps = dispatch =>{
  return {
    hideModalUploader : () => dispatch(hideUploader()),
    uploadFile: (event) => {
      let file = event.target.files[0];
      let formData = new FormData();
      formData.append('file', file);
      return dispatch(uploadFile(formData))
    }
  }
}

class Uploader extends Component {

  uploadFile(e) {
    if (!document.location.pathname.startsWith('/inspirations/')) {
      history.push('/inspirations/');
    }
    return this.props.uploadFile(e);
  }

  render() {

    return (
      <div className="Uploader">
       <div className={this.props.visible ? 'visible' : 'hidden'}>
        <div className="modal">
          <div className="cancel-cross" onClick={this.props.hideModalUploader}></div>
          <div className="inner">
            <h3 className="title">Use your own picture</h3>
            <p className="desc-text">Upload the picture of interior design which inspires you. We’ll do the rest</p>
            <div className="input-wrapper">
              <input type="file" onChange={this.uploadFile.bind(this)}/>
              <div className="input-description">
                <img src={chairImg} id="target" />
                <p >Drag and drop</p>
                <p >your picture anywhere here</p>
                <p >or</p>
                <button>Choose from computer</button>
              </div>
            </div>
            <p className="desc-text">Your jpg or png image can't be larger than 4000px in width and height, and must be smaller than 5mb in file size.</p>
          </div>
        </div>

       </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Uploader);
