import React from 'react';
import ReactDOM from 'react-dom';
import CategoryFilter from './CategoryFilter';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<CategoryFilter />, div);
});
