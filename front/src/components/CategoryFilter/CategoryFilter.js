import React, { Component } from 'react';
import './CategoryFilter.scss';
import store from '../../store';
import elipsis from '../../assets/elipsis.svg';
import { setCategory, showCategoryFilter, hideCategoryFilter } from '../../actions/inspirationActions';


class CategoryFilter extends Component {

  constructor(props) {
    super(props);
    this.state = {isVisible: false}
    this.unsubscribe = store.subscribe(() => {
      const state = store.getState()
      const categories = new Set(state.inspirationsReducer.gallery.photos.map(p => p.category))
      this.setState({
        categories: [...categories] || [],
        currentFilter: state.inspirationsReducer.categoryFilter.value,
        isVisible: state.inspirationsReducer.categoryFilter.visible
      })
    })
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  setCategoryFilter(category) {
    store.dispatch(setCategory(category));
  }

  toggleCategory() {
    if (this.state.isVisible) {
      store.dispatch(hideCategoryFilter())
    } else {
      store.dispatch(showCategoryFilter())
    }
  }

  render() {
    if (this.state && this.state.categories) {
      var availableCategories = this.state.categories.map(cat =>
        <li onClick={this.setCategoryFilter.bind(this, cat)} key={cat}>
          <div className={cat === this.state.currentFilter ? 'active' : ''}>{cat}</div>
        </li>
      )
      var selectedCategory = this.state.currentFilter || 'All categories';
    }

    return (
      <div className="CategoryFilter">
        <div className="text" onClick={this.toggleCategory.bind(this)}>
          {selectedCategory} <img src={elipsis} />
        </div>
        {this.state.isVisible &&
        <div className="content">
          <ul>
            <li onClick={this.setCategoryFilter.bind(this, false)}>
              <div className={false === this.state.currentFilter ? 'active' : ''}>All categories</div>
            </li>
            {availableCategories}
          </ul>
        </div>
        }
      </div>
    );
  }
}

export default CategoryFilter;
