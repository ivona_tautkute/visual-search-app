import React, { Component } from 'react';
import './Menu.scss';
import cameraIcon from '../../assets/icon-camera.svg';
import galleryIcon from '../../assets/icon-gallery.svg';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { showUploader } from '../../actions/inspirationActions';


const mapStateToProps = state => {
  return {
  }
}

const mapDispatchToProps = dispatch =>{
  return {
    showModalUploader : () => dispatch(showUploader())
  }
}

class Menu extends Component {
  render() {

    return (
      <div className="Menu">
        <div className="menu-item">
           <Link to="/"><img src={galleryIcon} /> INSPIRATIONS GALLERY</Link>
        </div>
        <div className="menu-item" onClick={this.props.showModalUploader}>
          <img src={cameraIcon} /> YOUR OWN IMAGE
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
