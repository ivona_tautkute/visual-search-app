import React, { Component } from 'react';
import './SearchReady.scss';
import { connect } from 'react-redux';
import cameraImg from '../../assets/camera.png';
import Sticky from 'sticky-js';

const mapStateToProps = state => {
  return { isVisible: false };
}

const mapDispatchToProps = dispatch =>{
  return {}
}

class SearchReady extends Component {

  componentDidMount() {
    new Sticky('.SearchReady');
  }

  render() {
    return (
      <div className="SearchReady" data-sticky-class="stuck">
        <h3>Let's start</h3>
        <img src={cameraImg} />
        <p>Find furniture that fits your chosen interior</p>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchReady);
