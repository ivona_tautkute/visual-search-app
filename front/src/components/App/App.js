import React, { Component } from 'react';
import './App.scss';
import { connect } from 'react-redux';
import GalleryView from '../GalleryView/GalleryView';
import TooplooxLabs from '../TooplooxLabs/TooplooxLabs';
import InspirationView from '../InspirationView/InspirationView';
import Menu from '../Menu/Menu';
import Uploader from '../Uploader/Uploader';
import {
  Router,
  Route
} from 'react-router-dom';
import Sticky from 'sticky-js';
import history from '../../history';


const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch =>{
  return {}
}


class App extends Component {

  render() {
    new Sticky('.menu-container');

    return (
      <Router history={history}>
        <div className="App" data-sticky-container>
          <Uploader />
          <TooplooxLabs />
          <section className="bg">
            <div className="bg-left"></div>
            <div className="bg-right"></div>
          </section>
          <div className="container menu-container">
            <Menu />
          </div>
          <div className="container gallery-container" >
            <Route exact path="/" component={GalleryView} />
            <Route path="/inspirations/:image?" component={InspirationView} />
          </div>
        </div>
      </Router>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
