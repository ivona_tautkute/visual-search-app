import React, { Component } from 'react';
import './Gallery.scss';
import store from '../../store';
import { fetchGallery } from '../../actions/inspirationActions';
import Tile from '../Tile/Tile';
import spinner from '../../assets/spinner.gif';


class Gallery extends Component {
  constructor(props) {
    super(props)
    this.state = {gallery: []}

    this.unsubscribe = store.subscribe(() => {
      const state = store.getState();
      this.setState({
        gallery: state.inspirationsReducer.gallery.photos,
        fetching: state.inspirationsReducer.gallery.fetching,
        categoryFilter: state.inspirationsReducer.categoryFilter.value
      })
    })
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  componentDidMount() {
    store.dispatch(fetchGallery());
  }

  render() {
    const tiles = this.state.gallery.filter(photo => {
      if(this.state.categoryFilter) {
        return photo.category === this.state.categoryFilter
      } else {
        return true
      }
    }).map(photo => <Tile photo={photo} key={photo.id} />)
    return (
      <div className="Gallery">
        {this.state.fetching ? <div className="spinner"><img src={spinner} /></div> : tiles}
      </div>
    );
  }
}

export default Gallery;
