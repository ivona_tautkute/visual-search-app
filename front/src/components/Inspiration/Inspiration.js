import React, { Component } from 'react';
import './Inspiration.scss';
import { connect } from 'react-redux';
import store from '../../store';
import arrowImg from '../../assets/arrow.svg';
import {
  fetchInspirations,
  selectInspirationItem,
  fetchSimilarObjects,
  fetchComplementaryObjects
} from '../../actions/inspirationActions';

const mapStateToProps = state => {
  return {
    foundItems: state.inspirationsReducer.inspiration.data.objects,
    framedImage: state.inspirationsReducer.inspiration.data.framedImage,
    queryImage: state.inspirationsReducer.inspiration.data.queryImage,
    selectedItem: state.inspirationsReducer.inspiration.item,
    fetching: state.inspirationsReducer.inspiration.fetching,
  };
}

const mapDispatchToProps = dispatch =>{
  return {
    selectInspirationItem : (item) => dispatch(selectInspirationItem(item)),

    fetchSimilarObjects: (item) => {
      const filename = item.imageUrl.slice(item.imageUrl.lastIndexOf('/') + 1);
      return dispatch(fetchSimilarObjects(filename))
    },

    fetchComplementaryObjects: (item) => {
      const filename = item.imageUrl.slice(item.imageUrl.lastIndexOf('/') + 1);
      return dispatch(fetchComplementaryObjects(filename))
    }
  }
}

class Inspiration extends Component {

  render() {
    const foundItems = this.props.foundItems.map(imgObj => {
      return <div
        key={imgObj.imageUrl}
        className={'found-item ' + (this.props.selectedItem === imgObj ? 'active' : 'inactive')}
        onClick={() => {this.props.selectInspirationItem(imgObj);  this.props.fetchSimilarObjects(imgObj); this.props.fetchComplementaryObjects(imgObj);}} ><img src={imgObj.imageUrl} />
      </div>
    })

    return (
      <div className="Inspiration">
        <div className="inspiration-box">
          <img src={this.props.framedImage} />
          <div className="found-items">{foundItems}</div>
          <div className="nav-wrapper">
            <div className="nav">
              <button><img src={arrowImg} /></button>
              <button><img src={arrowImg} /></button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Inspiration);
