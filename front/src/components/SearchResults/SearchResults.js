import React, { Component } from 'react';
import './SearchResults.scss';
import store from '../../store';
import Sticky from 'sticky-js';
import arrowImg from '../../assets/arrow.svg';
import { searchRefinedObjects } from '../../actions/inspirationActions';


class SearchResults extends Component {

  constructor(props) {
    super(props);
    const state = store.getState();
    this.state = {
      selectedItem: state.inspirationsReducer.inspiration.item,
      categoryFilter: state.inspirationsReducer.categoryFilter.value,
      similarObjects: [],
      complementaryObjects: [],
      itemsType: 'similar',
      selectedMatchingItem: null,
      searchText: ''
    }
  }

  componentDidMount() {
    const sticky = new Sticky('.SearchResults');
    this.unsubscribe = store.subscribe(() => {
      const state = store.getState();
        this.setState({
          selectedItem: state.inspirationsReducer.inspiration.item,
          complementaryObjects: state.inspirationsReducer.inspiration.complementaryObjects || [],
          similarObjects: state.inspirationsReducer.inspiration.similarObjects || []
        })
    })
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  setItemsType(type) {
    this.setState({
      itemsType: type
    })
  }

  setMatchingItem(item) {
    this.setState({
      selectedMatchingItem: item
    })
  }

  handleSearchTextChange(e) {
    this.setState({
      searchText: e.target.value.trim()
    })
  }

  search() {
    if (this.state.searchText.length > 1) {
      const fileUrl = this.state.similarObjects[0].imageUrl
      const filename = fileUrl.slice(fileUrl.lastIndexOf('/') + 1);
      store.dispatch(searchRefinedObjects(
        filename,
        this.state.searchText
      ))
    }
  }

  render() {

    let itemsObjects;
    if (this.state.itemsType === 'similar') {
      itemsObjects  = this.state.similarObjects;
    } else {
      itemsObjects = this.state.complementaryObjects;
    }
    const items = itemsObjects.map((obj, index) => {
      return <div key={index} className="item" onClick={this.setMatchingItem.bind(this, obj)}>
        <div className="item-thumbnail"><img src={obj.imageUrl} /></div>
        <p className="thumbnail-type">{this.state.selectedItem.objectType}</p>
        <p className="thumbnail-name">{obj.name}</p>
      </div>
    })

    let title;
    if (this.state.selectedMatchingItem) {
      title = <div className="go-back" onClick={this.setMatchingItem.bind(this, null)}><img src={arrowImg} /><p>Back to search results</p></div>
    } else {
      title = <p>Search results for selected object: <b>{this.state.selectedItem.objectType}</b></p>
    }

    let bigImage;
    if (this.state.selectedMatchingItem) {
      bigImage = <img src={this.state.selectedMatchingItem.imageUrl} />
    } else {
      bigImage = <img src={this.state.selectedItem.imageUrl} />
    }

    let matchingItemDescription;
     if (this.state.selectedMatchingItem) {
      matchingItemDescription = <div className="matching-item">
        <p className="type">{this.state.selectedItem.objectType} </p>
        <p className="name">{this.state.selectedMatchingItem.name}</p>
        <p className="description">{this.state.selectedMatchingItem.description}</p>
        <p className="more">More like this</p>

      </div>
    } else {
      matchingItemDescription = ''
    }

    let menu;
    if (this.state.selectedMatchingItem) {
      menu = ''
    } else {
      menu = <div className="menu">
        <div onClick={this.setItemsType.bind(this, 'similar')} className={'similar' === this.state.itemsType ? 'menu-item active' : 'menu-item'}>Similar items</div>
        <div onClick={this.setItemsType.bind(this, 'complementary')} className={'complementary' === this.state.itemsType ? 'menu-item active' : 'menu-item'}>Matching items</div>
      </div>
    }
    return (
      <div className="SearchResults" data-sticky-class="stuck">
      {this.state.selectedItem && <div className="wrapper">
        <div className="title">
          {title}
        </div>
        <div className="item-image">
          {bigImage}
        </div>
        {matchingItemDescription}
        <div className="search">
          <input type="text" placeholder="Add a text query (e.g black) ..."  onChange={ this.handleSearchTextChange.bind(this) }/>
          <button onClick={this.search.bind(this)}>Search</button>
        </div>
        {menu}
        <div className="items">
          {items}
       </div>
     </div>
      }
      </div>
    );
  }
}

export default SearchResults;
