import React, { Component } from 'react';
import './Tile.scss';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const mapStateToProps = state => {
  return {}
}

const mapDispatchToProps = dispatch =>{
  return {}
}

class Tile extends Component {

  render() {
    return (
      <div className="Tile">
        <Link onClick={() => window.scrollTo(0, 370)} to={`/inspirations/${this.props.photo.filename}`}>
          <div className="thumbnail">
            <img src={this.props.photo.imageUrl} alt={this.props.photo.category}></img>
          </div>
        </Link>
        <div className="description">
          <h3>{this.props.photo.category}</h3>
          <p>Producer: IKEA</p>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Tile);
