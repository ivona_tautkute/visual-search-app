export default function reducer(state={
  gallery: {
    fetching: false,
    rejected: false,
    photos: []
  },
  inspiration: {
    fetching: false,
    rejected: false,
    data: {
      objects: [],
      framedImage: {},
      queryImage: {},
      photos:[],
      category:[]
    },
    item: {
      imageUrl: '',
      objectType: '',
      matchProbability: {},
      frame: {
        x: {},
        y: {},
        width: {},
        height: {}
      },
      similarObjectsUrl: '',
      complementaryObjectsUrl: ''
    },
    similarObjects: [],
    complementaryObjects: [],
    showTips: true
  },
  categoryFilter: {
    value: null,
    visible: false
  },
  uploader: {
    visible: false,
    pending: false
  }
}, action) {
  switch (action.type) {
    case 'FETCH_GALLERY_FULFILLED': {
      return {...state, gallery: {fetching: false, rejected: false, photos: action.payload.data.photos}}
    }
    case 'FETCH_GALLERY_PENDING': {
      return {...state, gallery: {fetching: true, rejected: false, photos: []}, categoryFilter: {value: null, visible: false}}
    }
    case 'FETCH_GALLERY_REJECTED': {
      return {...state, gallery: {fetching: false, rejected: true, photos: []}}
    }
    case 'FETCH_INSPIRATIONS_FULFILLED': {
      let item = state.inspiration.showTips ? action.payload.data.objects[0] : null;
      return {...state, inspiration: {...state.inspiration, fetching: false, rejected: false, data: action.payload.data, item: item}, categoryFilter: {value: action.payload.data.category, visible: false}}
    }
    case 'FETCH_INSPIRATIONS_PENDING': {
      return {...state, inspiration: {...state.inspiration, fetching: true, rejected: false}}
    }
    case 'FETCH_INSPIRATIONS_REJECTED': {
      return {...state, inspiration: {fetching: false, rejected: true, data: null}}
    }
    case 'SET_CATEGORY_FILTER': {
      return {...state, categoryFilter: {value: action.payload, visible: false}}
    }
    case 'SHOW_CATEGORY_FILTER': {
      return {...state, categoryFilter: {value: state.categoryFilter.value, visible: true}}
    }
    case 'HIDE_CATEGORY_FILTER': {
      return {...state, categoryFilter: {value: state.categoryFilter.value, visible: false}}
    }
    case 'SELECT_INSPIRATION_ITEM': {
      return {...state, inspiration: {...state.inspiration, showTips: false, item: action.payload}}
    }
    case 'FETCH_SIMILAR_OBJECTS_FULFILLED': {
      return {...state, inspiration: {...state.inspiration, similarObjects: action.payload.data.similarObjects}}
    }
    case 'FETCH_SIMILAR_OBJECTS_PENDING': {
      return {...state, inspiration: {...state.inspiration, fetching: true}}
    }
    case 'FETCH_SIMILAR_OBJECTS_REJECTED': {
      return {...state, inspiration: {...state.inspiration, fetching: false}}
    }
    case 'FETCH_COMPLEMENTARY_OBJECTS_FULFILLED': {
      return {...state, inspiration: {...state.inspiration, complementaryObjects: action.payload.data.complementaryObjects}}
    }
    case 'FETCH_COMPLEMENTARY_OBJECTS_PENDING': {
      return {...state, inspiration: {...state.inspiration, fetching: true}}
    }
    case 'FETCH_COMPLEMENTARY_OBJECTS_REJECTED': {
       return {...state, inspiration: {...state.inspiration, fetching: false}}
    }
    case 'SHOW_UPLOADER': {
      return {...state, uploader: {...state.uploader, visible: true}}
    }
    case 'HIDE_UPLOADER': {
      return {...state, uploader: {...state.uploader, visible: false}}
    }
    case 'UPLOAD_FILE_PENDING': {
      return {...state, uploader: {...state.uploader, pending: true}}
    }
    case 'UPLOAD_FILE_FULFILLED': {
      return {...state, uploader: {...state.uploader, pending: false, visible: false}, inspiration: {...state.inspiration, data: action.payload.data, item: null}}
    }
    case 'UPLOAD_FILE_REJECTED': {
      return {...state, uploader: {...state.uploader, pending: false}}
    }
    case 'SEARCH_REFINED_OBJECTS_PENDING': {
      return state
    }
    case 'SEARCH_REFINED_OBJECTS_FULFILLED': {
      return {...state, inspiration: {...state.inspiration, similarObjects: action.payload.data.refinedObjects}}
    }
  }
  return state
}
