import { combineReducers } from 'redux';
import inspirationsReducer from './inspirationsReducer';


export default combineReducers({
  inspirationsReducer,
})