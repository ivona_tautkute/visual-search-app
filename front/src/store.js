import { applyMiddleware, createStore, compose }  from 'redux';
import promise from 'redux-promise-middleware';
import reducer from './reducers';
import thunk from 'redux-thunk';


const middleware = applyMiddleware(promise(), thunk);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export default createStore(reducer, composeEnhancers(middleware));