import gensim
import keras
from keras.models import Model
import numpy as np
import nltk
from nltk.corpus import stopwords
import pickle
import string

from cnn_feature_extraction import extract_features_cnn

class MyTokenizer:
    """
    Class for processing text queries.
        Takes a string and returns the list of appr opriate words
    """
    def __init__(self):
        pass
    
    def fit(self, X, y=None):
        return self
    
    def preprocess(self, text):
       
        translator = str.maketrans('', '', string.punctuation)
        my_stop_words = ('and','by')
        numbers = str(list(range(1, 101))).translate(translator).split()
        stop_words = stopwords.words('english') + numbers
        text = text.translate(translator) 
        text= text.lower().split() # separate words and make them lowercase
        text = [word for word in text if word not in stop_words]
    
        return text

    def transform(self, X):
        
        transformed_X = np.array( self.preprocess(X) )
        
        return transformed_X
    
    def fit_transform(self, X, y=None):
        return self.transform(X)

class MeanEmbeddingVectorizer(object):
    """
    Class producing list of single vectcor embeddings for each sentence from a provided list of sentences.
        Transform a text string into the vector computed by aveging the word2vec representations of single words
    """
    def __init__(self, word2vec):
        self.word2vec = word2vec
        
        self.dim = len(word2vec.wv.syn0[0])

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        
        X = np.array(MyTokenizer().fit_transform(X))
        words_known = []
        for word in X:
            if word in self.word2vec.wv.vocab:        
                words_known += [word]
        vec = [self.word2vec.wv[w] for w in words_known ] 
        mean_vec = np.mean(vec, axis =0)

        return np.array(mean_vec)
    
    def fit_transform(self, X, y=None):
        return self.transform(X)

nltk.download('stopwords')

with open('pickles/ikea_image_features.p', 'rb') as handle:
    ikea_image_features = pickle.load(handle)
with open('pickles/multimodal_features_web.p', 'rb') as handle:
    multimodal_features = pickle.load(handle)
with open('pickles/furniture_database_web.p', 'rb') as handle:
    furniture_database = pickle.load(handle)


word2vec = 'models/word2vec_model_desc_noid_mc1'
w2v_model = gensim.models.Word2Vec.load(word2vec)
vectorizer = MeanEmbeddingVectorizer(w2v_model)

def get_text_features(word):
    return vectorizer.transform(word)

def closest_node(node, nodes, n):
    nodes = np.asarray(nodes)
    dist_2 = np.sum((nodes - node)**2, axis=1)
    idx = np.argpartition(np.array(dist_2), n)
    return idx[:n]

model = keras.models.load_model('models/classification_model_web.h5')
multimodal_model = Model(model.inputs, model.get_layer('concatenated').output)

def get_multimodal_features(test_product, test_query):
    try: 
        test_image = ikea_image_features[test_product].reshape((1,2048))
    except KeyError:
        test_image = extract_features_cnn(os.path.join('static/images/all_images/', test_product))
    test_text = (get_text_features(test_query)).reshape((1, 260))
    test_features = multimodal_model.predict([test_image, test_text])
    return test_features

def get_closest_multimodal(test_product, test_query, n_items = 3):
    test_features = get_multimodal_features(test_product, test_query)
    results = [furniture_database[i] for i in closest_node(test_features, multimodal_features, n_items)]
    print(results)
    return results

if __name__ ==  "__main__":	    
    get_closest_multimodal('502.675.58.jpg', 'black')
