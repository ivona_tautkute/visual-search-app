"""Web application interface
"""

import os
import itertools as it
from functools import partial
import numpy as np
import random
import json

from random import sample
from flask import render_template, request, flash, session, url_for
from flask_restful import Api, Resource
from PIL import Image
from werkzeug.utils import secure_filename

from app import (app, search_engine, ENGINE_BED, ENGINE_CHAIR, ENGINE_CLOCK,
                 ENGINE_COUCH, ENGINE_TABLE, ENGINE_OBJECTS, img_to_desc,
                 categories_dict, categories_images_dict, CNN_FEATURES)
from cnn_feature_extraction import save_image_features
from multimodal_features import get_closest_multimodal
from main import return_similar, timeit
from object_detection_tensorflow import (
    get_bounding_boxes, get_most_probable_category, save_cropped_bounding_box,
    load_object_detection_model, load_labels_map, run_object_detection
)

category_index = load_labels_map(app.config['PATH_TO_LABELS_MAP'],
                                 nb_of_classes=app.config['NB_OF_CLASSES'])

detection_graph = load_object_detection_model(app.config['PATH_TO_PB_MODEL'])

@app.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', '*')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization')
  response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
  return response

def get_directories(object_class=None):
    """Returns absolute/static directories to all images of given class and location to model of extracted
        visual features for given object class"""
    if object_class == "bed":
        return app.config['BED_DIR'], ENGINE_BED, '/static/images/bed'
    elif object_class == "chair":
        return app.config['CHAIR_DIR'], ENGINE_CHAIR, '/static/images/chair'
    elif object_class == "clock":
        return app.config['CLOCK_DIR'], ENGINE_CLOCK, '/static/images/clock'
    elif object_class == "dining table":
        return app.config['TABLE_DIR'], ENGINE_TABLE, '/static/images/dining table'
    elif object_class == "couch":
        return app.config['COUCH_DIR'], ENGINE_COUCH, '/static/images/couch'
    else:
        return app.config['OBJECTS_DIR'], ENGINE_OBJECTS, '/static/images/objects'

@timeit
def get_clicked_object(base_image_path, bound_boxes, image_x, image_y, width):
    """ For image with bounding boxes that were detected on it and user click x, y coordinates on image
        returns the bounding box that was clicked by user (0 if no valid bounding box was clicked)"""
    with Image.open(base_image_path) as img:
        img_width, _ = img.size
    # rescale x and y coordinates accordingly
    print('img_width', img_width)
    print('width', width)
    print('image_x', image_x)
    rescale_factor = int(img_width) / int(width)
    if rescale_factor != 1.0:
        image_x = float(image_x) * rescale_factor
        image_y = float(image_y) * rescale_factor
    for box in bound_boxes:
        if box[0] not in app.config['ALLOWED_CLASSES']:
            continue
        else:
            x1 = int(box[1])
            x2 = int(box[2])
            y1 = int(box[3])
            y2 = int(box[4])
            if float(x1) < float(image_x) and float(image_x) < float(x2):
                if float(y1) < float(image_y) and float(image_y) < float(y2):
                    return box
    return 0

@timeit
def object_class_blender(results, desired_class):
    """For an array of results (paths to result images) returns only those
    that belong to required object class
    """
    class_results = []
    for entry in results:
        entry_class = entry.split("/")[2]
        print('Entry class:', entry_class)
        if entry_class == desired_class:
            class_results.append(entry)
    return class_results

@timeit
def feature_blender(initial_image, w2vec_results, countvect_results, output_nb=4):
    """Gets query image (path) and text search results (w2vec and countvectorizer).
    Returns static paths to images from text search that match visual features of query image the best.
    """
    initial_query = w2vec_results + countvect_results
    initial_features = save_image_features(initial_image,
                                           app.config['FEATURE_MODEL'],
                                           app.config['CNN_FEATURES_FILE'])
    distances = {}
    for product_path in initial_query:
        product_features = save_image_features(product_path,
                           app.config['FEATURE_MODEL'],
                           app.config['CNN_FEATURES_FILE'])
        if product_features is not None:
            distances[product_path] = np.linalg.norm(product_features - initial_features)
    sorted_array = [key.replace('app','') for key in sorted(distances, key=distances.__getitem__)]
    return sorted_array[:output_nb]

@timeit
def complementary_objects_search(object_image_path, object_class):
    """Takes image of an object (cropped) and its category.
        Returns static paths to similar images from other categories"""
    complementary_objects = []
    return_classes = {
        'chair': {'couch': 2, 'dining table' : 2, 'objects': 2},
        'couch': {'chair': 2, 'clock': 2, 'objects': 2},
        'bed': {'chair': 2, 'clock': 2, 'objects': 2},
        'dining table': {'chair': 3, 'objects': 3},
        'clock': {'couch': 2, 'dining table': 2, 'objects': 2}
    }
    default_return_class = {'objects': 6}

    for category, number in return_classes.get(object_class, default_return_class).items():
        search_dir_compl, engine_compl, static_path_compl = get_directories(category)
        similar_objects = return_similar(
            object_image_path,
            search_dir_compl,
            engine_compl,
            app.config['FEATURE_MODEL'],
            CNN_FEATURES,
            app.config['CNN_FEATURES_FILE'],
            number
        )
        complementary_objects.extend([os.path.join(static_path_compl, obj)
                                      for obj in similar_objects.keys()])

    return complementary_objects

def refine_results(product_image, text_query):
    return get_closest_multimodal(product_image, text_query)


@timeit
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

# API #########################################################################

external_url = partial(url_for, _external=True)
asset_url = partial(external_url, 'static')


class Healthcheck(Resource):

    def get(self):
        return {'status': 'ok'}


class RoomScenesList(Resource):

    def get(self):
        counter = it.count(1)
        images = []
        for filename in os.listdir(app.config['ROOMS_DIR']):
            if not filename.endswith('.jpg'):
                continue
            images.append({
                'id': next(counter),
                'imageUrl': asset_url(filename=os.path.join(
                    'images/room_scenes', filename)),
                'filename': filename,
                'detailsUrl': external_url('scene-details', filename=filename),
                'category': categories_dict.get(filename, 'uncategorized'),
            })
        return {
            'photos': images,
        }

    def post(self):
        posted_image = request.files['file']
        image_name = secure_filename(posted_image.filename)
        image_path = os.path.join(app.config['UPLOAD_FOLDER'], image_name)
        posted_image.save(image_path)
        bounding_boxes = get_bounding_boxes(
            image_path,
            category_index,
            detection_graph,
            app.config['BOUNDING_BOXES'],
            min_score_thresh=app.config['MIN_THRESHOLD']
        )
        cropped_images = []
        for box in bounding_boxes:
            category, xmin, xmax, ymin, ymax, probability = box
            if category not in app.config['ALLOWED_CLASSES']:
                continue
            cropped_image_path = save_cropped_bounding_box(
                image_path,
                box,
                app.config['BOUNDING_BOXES']
            )
            cropped_image_filename = os.path.basename(cropped_image_path)
            image_url = asset_url(filename=os.path.join(
                'bounding_boxes', cropped_image_filename))
            cropped_images.append(
                {
                    'imageUrl': image_url,
                    'objectType': category,
                    'matchProbability': probability.item(),
                    'frame': {
                        'x': xmin,
                        'y': ymin,
                        'width': xmax - xmin,
                        'height': ymax - ymin
                    },
                    'similarObjectsUrl': external_url(
                        'similar-list',
                        filename=cropped_image_filename
                    ),
                    'complementaryObjectsUrl': external_url(
                        'complementary-list',
                        filename=cropped_image_filename
                    )
                }
            )

        return {
            'queryImage': asset_url(filename=os.path.join(
                'uploads', image_name)),
            'framedImage': asset_url(filename=os.path.join(
                'bounding_boxes', 'predictions_' + image_name)),
            'objects': cropped_images
        }


class RoomSceneDetails(Resource):

    def get(self, filename):
        base_image_path = os.path.join(
            app.config['ROOMS_DIR'], filename)
        bounding_boxes = get_bounding_boxes(
            base_image_path,
            category_index,
            detection_graph,
            app.config['BOUNDING_BOXES'],
            min_score_thresh=app.config['MIN_THRESHOLD']
        )

        cropped_images = []
        for box in bounding_boxes:
            category, xmin, xmax, ymin, ymax, probability = box
            if category not in app.config['ALLOWED_CLASSES']:
                continue
            cropped_image_path = save_cropped_bounding_box(
                base_image_path,
                box,
                app.config['BOUNDING_BOXES']
            )
            cropped_image_filename = os.path.basename(cropped_image_path)
            image_url = asset_url(filename=os.path.join(
                'bounding_boxes', cropped_image_filename))
            cropped_images.append(
                {
                    'imageUrl': image_url,
                    'objectType': category,
                    'matchProbability': probability.item(),
                    'frame': {
                        'x': xmin,
                        'y': ymin,
                        'width': xmax - xmin,
                        'height': ymax - ymin
                    },
                    'similarObjectsUrl': external_url(
                        'similar-list',
                        filename=cropped_image_filename
                    ),
                    'complementaryObjectsUrl': external_url(
                        'complementary-list',
                        filename=cropped_image_filename
                    )
                }
            )

        return {
            'queryImage': asset_url(filename=os.path.join(
                'images/room_scenes', filename)),
            'framedImage': asset_url(filename=os.path.join(
                'bounding_boxes', 'predictions_' + filename)),
            'objects': cropped_images,
            'category': categories_dict.get(filename, 'uncategorized'),

        }


class SimilarObjectsList(Resource):

    def get(self, filename):
        image_url = asset_url(filename=os.path.join(
            'bounding_boxes', filename))
        category, _ = filename.split('_', 1)
        search_dir, engine, static_path = get_directories(category)
        similar_objects = return_similar(
            os.path.join(app.config['BOUNDING_BOXES'], filename),
            search_dir,
            engine,
            app.config['FEATURE_MODEL'],
            CNN_FEATURES,
            app.config['CNN_FEATURES_FILE'],
            app.config['NB_MATCHES']
        )

        return {
            'imageUrl': image_url,
            'objectType': category,
            'similarObjects': [
                {
                    'imageUrl': asset_url(filename=os.path.join(
                        *static_path.split(os.path.sep)[2:], obj)),
                    'name': (img_to_desc
                             .get(os.path.join(static_path, obj), {})
                             .get('name')),
                    'description': (img_to_desc
                                    .get(os.path.join(static_path, obj), {})
                                    .get('desc')),
                }
                for obj in similar_objects
            ]
        }


class ComplementaryObjectsList(Resource):

    def get(self, filename):
        image_url = asset_url(filename=os.path.join(
            'bounding_boxes', filename))
        category, _ = filename.split('_', 1)
        complementary_objects = complementary_objects_search(
            os.path.join(app.config['BOUNDING_BOXES'], filename),
            category
        )
        return {
            'imageUrl': image_url,
            'objectType': category,
            'complementaryObjects': [
                {
                    'imageUrl': asset_url(filename=os.path.join(
                        *obj.split(os.path.sep)[2:])),
                    'name': img_to_desc.get(obj, {}).get('name'),
                    'description': img_to_desc.get(obj, {}).get('desc'),
                }
                for obj in complementary_objects
            ]
        }


class RefinedObjectsList(Resource):

    def get(self, filename, text_query):
        image_url = asset_url(filename=os.path.join(
            'bounding_boxes', filename))
        # category, _ = filename.split('_', 1)
        refined_objects = [filename] + refine_results(os.path.basename(filename), text_query)
        print([asset_url(filename=obj)] for obj in refined_objects)
        return {
            'imageUrl': image_url,
            # 'objectType': category,
            'refinedObjects': [
                {
                    'imageUrl': asset_url(filename=os.path.join(
                        'images/all_images', obj)),
                    'name': img_to_desc.get(obj, {}).get('name'),
                    'description': img_to_desc.get(obj, {}).get('desc'),
                }
                for obj in refined_objects
            ]
        }


api = Api(app, prefix='/api/v1')
api.add_resource(Healthcheck, '/', endpoint='healthcheck')
api.add_resource(RoomScenesList, '/scenes', endpoint='scenes-list')
api.add_resource(RoomSceneDetails, '/scenes/<string:filename>',
                 endpoint='scene-details')
api.add_resource(SimilarObjectsList, '/similar/<string:filename>',
                 endpoint='similar-list')
api.add_resource(ComplementaryObjectsList, '/complementary/<string:filename>',
                 endpoint='complementary-list')
api.add_resource(
    RefinedObjectsList,
    '/refined/<string:filename>/<string:text_query>',
    endpoint='refined-list'
)

###############################################################################

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    return render_template('index.html')
