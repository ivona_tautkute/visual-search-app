from flask import Flask
from flask_restful import Resource, Api
import gensim
import os, os.path
import pickle
import keras.applications.vgg19
import keras.applications.vgg16
import keras.applications.resnet50
from keras.models import Model
import time

from main import initiate_engine, load

from text_training import CountVectModel
from text_search_engine import SearchEngine

#Initiate Flask app
app = Flask(__name__)
app.secret_key = os.urandom(12)


#Application directories configuration
app.config.from_object('config')
app.static_folder = 'static'
app.config['UPLOAD_FOLDER'] = './app/static/uploads'
app.config['BOUNDING_BOXES']='./app/static/bounding_boxes'
app.config['CNN_FEATURES_FILE'] = 'pickles/cnn_features_resnet.pickle'
app.config['ROOMS_DIR'] = './app/static/images/room_scenes'
app.config['BED_DIR'] = './app/static/images/bed'
app.config['CHAIR_DIR'] = './app/static/images/chair'
app.config['CLOCK_DIR'] = './app/static/images/clock'
app.config['COUCH_DIR'] = './app/static/images/couch'
app.config['TABLE_DIR'] = './app/static/images/dining table'
app.config['OBJECTS_DIR'] = './app/static/images/objects'

#Application settings
app.config['MAX_CONTENT_LENGTH'] = 10 * 1024 * 1024
app.config['ALLOWED_CLASSES'] = ['bed', 'chair', 'clock', 'couch', 'dining table', 'objects']
app.config['CLASS_COLORS'] = {"chair": "#2B98F0", "pottedplant":"#2B98F0", "sofa":"#2B98F0", "clock":"#2B98F0", "diningtable":"#2B98F0",
    "bed":"#2B98F0", "room":"#2B98F0"}
app.config['NB_MATCHES']=6
app.config['NB_OF_CLASSES']=90
app.config['ALLOWED_EXTENSIONS'] = set(['png', 'jpg', 'jpeg'])

#Feature extraction settings
app.config['FEATURE_MODEL'] = 'resnet'
app.config['CNN_LAYER'] = 'fc2'
model_extension = app.config['FEATURE_MODEL'] #Feature extraction model in [vgg16, vgg19, resnet]

#Object detection settings
app.config['MIN_THRESHOLD'] = 0.4
app.config['PATH_TO_LABELS_MAP'] = 'object_detection/data/mscoco_label_map.pbtxt'
app.config['PATH_TO_PB_MODEL'] = 'object_detection/ssd_inception_coco.pb'

# load dict
with open('pickles/products_dict.p', 'rb') as handle:
    products_dict = pickle.load(handle)

# load img_to_desc dict
with open('pickles/img_to_desc.p', 'rb') as handle:
    img_to_desc = pickle.load(handle)

# load categories_dict dict
with open('pickles/categories_dict.p', 'rb') as handle:
    categories_dict = pickle.load(handle)

# load categories_images_dict dict
with open('pickles/categories_images_dict.p', 'rb') as handle:
    categories_images_dict = pickle.load(handle)

# load w2vec model
with open('pickles/word2vec_model.p', 'rb') as f:
    model = pickle.load(f)

# build search engine
vectorizer = CountVectModel(products_dict)
search_engine = SearchEngine(products_dict, vectorizer, model)

#Load cnn features
try:
    with open('pickles/cnn_features_' + model_extension + '.pickle', 'rb') as f:
        CNN_FEATURES = pickle.load(f)
        print('CNN features loaded!')
except FileNotFoundError:
    print('No CNN features for model', model_extension)

#Load bounding boxes
try:
    with open('pickles/bounding_boxes.pickle', 'rb') as f:
        BOUNDING_BOXES_DETECTIONS = pickle.load(f)
        print('Bounding boxes loaded!')
except FileNotFoundError:
    print('No pre-detected bounding boxes!')

#Initiate deep features model
if model_extension == "vgg19":
    print('VGG19 not supported in web app!')
elif model_extension == "vgg16":
    print('VGG16 not supported in web app!')
elif model_extension == "resnet":
    model = keras.applications.resnet50.ResNet50(include_top=False)
else:
    raise Exception('Model name is not valid: ', model_extension)

# Initiate engines for different object categories
try:
    with open('pickles/bed_' + model_extension + '.pickle', 'rb') as handle:
        ENGINE_BED = pickle.load(handle)
except FileNotFoundError:
    ENGINE_BED = initiate_engine(app.config['BED_DIR'], model_extension)

try:
    with open('pickles/chair_' + model_extension + '.pickle', 'rb') as handle:
        ENGINE_CHAIR = pickle.load(handle)
except FileNotFoundError:
    ENGINE_CHAIR = initiate_engine(app.config['CHAIR_DIR'], model_extension)

try:
    with open('pickles/clock_' + model_extension + '.pickle', 'rb') as handle:
        ENGINE_CLOCK = pickle.load(handle)
except FileNotFoundError:
    ENGINE_CLOCK = initiate_engine(app.config['CLOCK_DIR'], model_extension)

try:
    with open('pickles/couch_' + model_extension + '.pickle', 'rb') as handle:
        ENGINE_COUCH = pickle.load(handle)
except FileNotFoundError:
    ENGINE_COUCH = initiate_engine(app.config['COUCH_DIR'], model_extension)

try:
    with open('pickles/dining table_' + model_extension + '.pickle', 'rb') as handle:
        ENGINE_TABLE = pickle.load(handle)
except FileNotFoundError:
    ENGINE_TABLE = initiate_engine(app.config['TABLE_DIR'], model_extension)

try:
    with open('pickles/objects_' + model_extension + '.pickle', 'rb') as handle:
        ENGINE_OBJECTS = pickle.load(handle)
except FileNotFoundError:
    ENGINE_OBJECTS = initiate_engine(app.config['OBJECTS_DIR'], model_extension)



print('All engines initiated')


from app import web_interface
