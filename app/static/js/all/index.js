jQuery(function($){

  var html,
  value;
  window.onload = function(){
    html = document.getElementById('upload_form').innerHTML;
    value = $('.form-group-big .image--category').attr('value');

    $( '.gallery__narrow .col' ).css({"display":"none"});
    $( '.gallery__narrow .image--category' ).filter(function( index ) {
      return $( this ).attr( "value" ) === value;
    }).addClass('active');

    $(".active").parents(".col").css({"display":"block"}).addClass('parent');

    if($('.form-group-big .image--category').attr('value')==0){
      $( '.gallery__narrow .col' ).css({"display":"block"});
    }
  };

  $('.modal').css({"display":"none"});
  $("#modal-trigger, #modal-trigger2").click(function(){
    $('.modal').css({"display":"flex"});
  });
  $("#close--white").click(function(){
    $('.modal').css({"display":"none"});
  });

  $("#uploaded_file").change(function(){
    $("#target").hide();
    var reader = new FileReader();
    reader.onload = function(e) {
      $("#target").show();
      $("#target").attr("src", e.target.result);
      $("#upload_form").submit();
      $("#upload_form").css({"pointer-events":"none"});
      $( "#load_text" ).text( "Your file is being processed - please wait a minute to see the results" );
      $("#load_text-bottom").text("");
      $(".hidden").removeClass("hidden").addClass("loader");
      $(".img-desc").addClass("hidden");
      $(".form__group").addClass("hidden");
    };

    $('#reset').click(function(e) {
      reader.abort(e);
      $("#target").attr("src", "/static/img/chair.svg");
      var container = document.getElementById("upload_form");
      container.innerHTML= html;
      $(".loader").removeClass("loader").addClass("hidden");
      window.location = 'scene_gallery';
    });

    if($("img[src*='/static/img/chair.svg']")){
      $(".img--responsive").css({"width":"100%","opacity":"0.4"});
    }

    reader.readAsDataURL($(this)[0].files[0]);
  });


  $(document).ready(function(){
    $('#mySpinner').addClass('spinner');
    $('body').css({"overflow":"hidden"});
    $("#detections_img").click(function(e){
      var offset = $(this).offset();
      document.updateobject_form.form_x.value = e.pageX - offset.left;
      document.updateobject_form.form_y.value = e.pageY - offset.top;
      document.updateobject_form.width.value = $(this).width();
    });

  });

});

$(window).load(function() {
  $('#mySpinner').removeClass('spinner');
  $('body').css({"overflow":"auto"});
  var location = window.location.href.match(/([^\/]*)\/*$/)[1];
  $('body').css({'opacity':'1'});
  if(location === "update_object" || location === "gallery_uploader" || location === "uploader" || location === "update_text"){
    $('body').css({'background-color':'#fff'});
  } else if(location === " "|| location === "scene_gallery"|| location === "login"){
    $('.menu').css({'border-bottom':'none'});
    $('ul .nav-item:first-child .nav-link').css({'color':'#2096f3'});
    $('ul .nav-item:first-child .nav-link img').attr('src', '/static/img/icon-gallery-active.svg');
  }
});
