jQuery(function($){

	var owl = $("#owl"),
			src = $('.image-container img').attr('src');

	owl.owlCarousel({
		items : 4
	});

	$(".next").click(function(){ owl.trigger('owl.next'); });
	$(".prev").click(function(){ owl.trigger('owl.prev'); });


	$( ".owl-item form #carousel_object_image" ).filter(function( index ) {
    return $( this ).attr( "src" ) === src;
  }).addClass('active').css({"border":"none"});

	$('.active:first').closest('.owl-item').addClass('active');

});
