jQuery(document).ready(function() {
  "use strict";
  var $ = jQuery,
  location = window.location.href.match(/([^\/]*)\/*$/)[1];
  if(location === "update_object"  || location === "gallery_uploader" || location === "uploader" || location === "update_text"){
    var topOfDiv = $(".container-right").offset().top,
    topOfPost = $(".update_object").offset().top,
    winHeight = $(window).height();

    $(window).scroll(function(){
      if($(window).scrollTop() > topOfPost){
        $(".container-right").css({"position":"fixed"}).css({"top":"0"});
      } else if ($(window).scrollTop() < topOfPost) {
        $(".container-right").offset( {top: topOfPost} );
      } else if($(window).scrollTop() <= topOfPost) {
        $(".container-right").css({"position":"absolute"}).css({"top":"0"});
      }
    });
  }

});
