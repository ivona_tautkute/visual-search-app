jQuery(function($){

var dataSrc = $('.image-container').find('img').attr("src");
  $('.col-centered').click(function() {
    var loc = $(this).find('img').attr("src");
    $('.image-container img').attr("src",loc);
    $('.col-sidebar').removeClass('hidden-menu');
    $('#title-1').addClass('hidden-menu');
    $('#title-2').removeClass('hidden-menu').css({"text-transform":"none"});
    $(".inner-grey").css({"min-height":"500px"});
    $('#name').html($(this).find('.name').text());
    $('#type').html($(this).find('.type').text());
    $('#desc').html($(this).find('.desc').text());
    $('#more').html("More like this:");
    $('.menu-small, .tab').css({"display":"none"});
    $('.container-right').animate({scrollTop:$('#container-right').position().top}, 800);
  });

  $('.arrow-back').click(function() {
    $('.image-container img').attr("src",dataSrc);
    $('.col-sidebar').addClass('hidden-menu');
    $('#title-1').removeClass('hidden-menu');
    $('#title-2').addClass('hidden-menu').css({"text-transform":"none"});
    $(".inner-grey").css({"min-height":"400px"});
    $('#name').html('');
    $('#type').html('');
    $('#desc').html('');
    $('#more').html('');
    $('.menu-small').css({"display":"flex"});
    $('.tab').css({"display":"block"});
  });


});
