jQuery(document).ready(function() {
  "use strict";

  var $ = jQuery;

    if (Cookies.get('hide')) $('.tooltiptext').hide();
    else {
      $(".btn-close, .tooltiptext").click(function() {
        $(".tooltiptext").fadeOut(1000);
        Cookies.get('hide', true);
      });

    }

});
