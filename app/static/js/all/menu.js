jQuery(function($){

  $(".dropdown-menu a").on("click", function (event) {
    $(".dropdown-menu a").removeClass('active');$(this).addClass('active');
    var category = $(this).attr("id");
    var photosInCategory = $(".gallery .group > div").filter(function (i, photo) {
      return $(photo).hasClass(category);
    });
    $(".gallery .group > div").css({"display":"none"});
    $(photosInCategory).css({"display":"block"});
  });

  $("#all").on("click", function (event) {
    $(".gallery .group > div").css({"display":"block"});
  });
  $('.dropdown-toggle').on("click", function (event){
    $('body').animate({scrollTop:$('#menu').position().top}, 800);
  });
  $('.dropdown-toggle').on("click", function (event){
    $('body').animate({scrollTop:$('#menu').position().top}, 800);
  });

  $(".nav-item").on("click", function (event) {
    $(".nav-item ").removeClass('active');$(this).addClass('active');
    var tab = $(this).attr("id");
    var itemsInTab = $(".col-centered").filter(function (i, item) {
      return $(item).hasClass(tab);
    });
    $(".col-centered").css({"display":"none"});
    $(itemsInTab).css({"display":"flex"});
  });

});
