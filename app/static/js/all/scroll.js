jQuery(function($){

  var scrollTop     = $(window).scrollTop(),
      elementOffset = $('#menu').offset().top,
      distance      = (elementOffset - scrollTop);

  $(window).bind('scroll', function() {
    if ($(window).scrollTop() > distance) {
       $('#menu').css({"position":"fixed", "box-shadow":"0 8px 22px 0 rgba(17, 28, 68, 0.16)"});
       }
       else {
        $('#menu').css({"position":"relative", "box-shadow":"none"});
       }
  });

});
